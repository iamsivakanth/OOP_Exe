package com.training.main;

import com.training.operations.Area;

public class MainClass {

	public static void main(String[] args) {
		Area a1 = new Area();
		a1.findAreaRect(5.5f, 7.6f);
		a1.findAreaSquare(9.5f);

		Area a2 = new Area();
		a2.findAreaRect(4.5f, 5.6f);
		a2.findAreaSquare(7.4f);
	}
}