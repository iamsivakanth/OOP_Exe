package com.training.operations;

public class Area {

	float ar;

	public void findAreaRect(float length, float width) {
		ar = length * width;
		display();
	}

	public void findAreaSquare(float length) {
		ar = length * length;
		display();
	}

	private void display() {
		System.out.println("AREA : " + ar);
	}
}